#!/usr/bin/perl

use strict;
use warnings;

my $CONFIG = '/etc/aoeclient.conf';
my $MIN_FIELDS = 5;   #  clname, disk, netdev, shelf, slot
my $MAX_FIELDS = 10;  #  mac, ip, status, userpass, comment

my $ETHERWAKE = '/usr/bin/etherwake';
my $NET_CMD   = '/usr/bin/net';
my $ZFS_CMD   = '/usr/bin/zfs';
my $AOE_ADM   = '/usr/local/sbin/aoeadm';

my $SOURCE_DRIVE = 'archive/client@snap';
my $DESTDRV_BASE = 'archive';
my $DESTDEV_BASE = '/dev/zvol/archive';

my $PING_COUNT = 2;
my $MAX_WAIT   = 3;

sub usage($) { die "Usage: $0 @_\n" }
sub common_usage() { usage "list|start|stop|restart|start-all|stop-all|enable|disable|create ..." }


#=======   Helpers  ====================================================

sub is_alive($) { `ping -qnc$PING_COUNT -w$MAX_WAIT $_[0] >/dev/null 2>&1 && echo -n 1 || echo -n 0` }

sub is_disabled($) { local $_ = shift; $_ ne '-' and not /^en/i }

sub execute {
	my $hint = shift;
	my $cmd = join(' ', map {"'$_'"} @_);
#	my $out = `echo $cmd 2>&1`;   # DEBUG
	my $out = `$cmd 2>&1`;
	chomp $out;
	printf "%s: %s\n", $hint, $out;
}


#=======   Config   ====================================================

sub config_reader($$) {
	my ($clname, $line_func, $notfound_func) = @_;
	open F, $CONFIG or die "Cannot read $CONFIG: $!\n";
	my $found;
	while(<F>) {
		chomp;
		s/\s#.*$//;       # strip comment tail
		next if /^\s*$/;  # skip empty lines
		next if /^#/;     # skip comment lines
		my @words = split;
		if (@words < $MIN_FIELDS or @words > $MAX_FIELDS) {
			warn "Wrong line $., skipped: $_\n";
			next;
		}

		$words[$_] //= '-' foreach 0..($MAX_FIELDS-1);

		unless ($clname) {
			$line_func->(@words);
		} elsif ($words[0] eq $clname) {
			$line_func->(@words);
			$found = 1;
			last;
		}
	}
	close F;
	warn "Unknown client $clname\n" if $clname and not $found;
}

sub config_changer($$;$) {
	my ($clname, $line_func, $notfound_func) = @_;
	open F, $CONFIG or die "Cannot read $CONFIG: $!\n";
	my ($changed, $found);
	my @lines;
	while(<F>) {
		chomp;
		push @lines, $_;
		next if /^\s*$/;  # skip empty lines
		next if /^#/;     # skip comment lines
		my $comment = ''; # todo: strip to here!

		my @words = split;
		if (@words < $MIN_FIELDS or @words > $MAX_FIELDS) {
			warn "Wrong line $., skipped: $_\n";
			next;
		}

		$words[$_] //= '-' foreach 0..($MAX_FIELDS-1);

		next if $clname ne $words[0];

		$found = 1;
		$changed = $line_func->(\@words);
		#printf "DEBUG: %s\n",
		$lines[$#lines] = "@words $comment\n";
		last;
	}
	close F;

	if ($found) {
	} elsif ($notfound_func) {
		my $added_line = $notfound_func->();
		$changed = 1 if defined $added_line;
		push @lines, "$added_line\n";
	} elsif ($clname) {
		warn "Unknown client $clname\n";
	}
	return if not $changed;

	open F, "> $CONFIG" or die "Cannot update $CONFIG: $!\n";
	print F $_ foreach @lines;
	close F;
}


#=======   Status   ====================================================

sub status() {
	my $fmt = "%-10s  %-10s  %8s  %6s  %6s  %-12s  %-14s  %s\n";
	printf $fmt,
		'Client',
		'Disk',
		'Netdev',
		'Shelf',
		'Slot',
		'MAC',
		'IP',
		'Status'
		;
	config_reader(undef, sub {
		# Check host is alive and update status..
		my $ipaddr = $_[6];
		$_[7] = is_disabled($_[7]) ? 'disabled' : ' enabled';
		if ($ipaddr eq '-') {
			$_[7] .= '/unknown';
		} elsif (is_alive($ipaddr)) {
			$_[7] .= '/ONLINE';
		} else {
			$_[7] .= '/Offline';
		}
		printf $fmt, @_[0..7];
	} );
}


#=======   Start/stop   ================================================

sub start($) {
	config_reader($_[0], sub {
		my ($clname, $netdev, $mac, $status) = ($_[0], $_[1], $_[4], $_[7]);
		return warn "Client $clname if disabled, enable first.\n"
			if is_disabled($status);
		return warn "Empty MAC, skip $clname.\n"
			if $mac eq '-';
		execute "Starting $clname", $ETHERWAKE, '-l', $netdev, $mac;
	} );
}

sub start_all() {
	config_reader(undef, sub {
		my ($clname, $netdev, $mac, $status) = ($_[0], $_[1], $_[4], $_[7]);
		return if is_disabled($status);
		return if $mac eq '-';
		execute "Starting $clname", $ETHERWAKE, '-l', $netdev, $mac;
	} );
}

sub stop($) {
	config_reader($_[0], sub {
		my ($clname, $ipaddr, $userpass, $comment) = ($_[0], $_[6], $_[8], $_[9]);
		return warn "Cannot stop $clname: missing IP.\n"        if $ipaddr eq '-';
		return warn "Cannot stop $clname: missing user/pass.\n" if $userpass eq '-';
		return warn "Cannot stop $clname: is $ipaddr alive?\n"  if !is_alive($ipaddr);
		my @cmd = ($NET_CMD, 'rpc', 'SHUTDOWN', '-f');
		push @cmd, '-C', $comment;
		push @cmd, '-I', $ipaddr;
		push @cmd, '-U', $userpass;
		execute "Stopping $clname", @cmd;
	} );
}

sub stop_all() {
	my $stopped = 0;
	config_reader(undef, sub {
		my ($clname, $ipaddr, $userpass, $comment) = ($_[0], $_[6], $_[8], $_[9]);
		return if $ipaddr eq '-';
		return if $userpass eq '-';
		return if !is_alive($ipaddr);
		my @cmd = ($NET_CMD, 'rpc', 'SHUTDOWN', '-f');
		push @cmd, '-C', $comment;
		push @cmd, '-I', $ipaddr;
		push @cmd, '-U', $userpass;
		execute "Stopping $clname", @cmd;
		$stopped++;
	} );
	return $stopped;
}

sub restart() {
	stop_all();
	start_all();
}

sub cond_restart() {
	stop_all() and start_all();
}


#=======   Enable/disable   ============================================

sub change_status($$) {
	my ($client_name, $new_status) = @_;
	config_changer( $client_name, sub {
		my $w = shift;
		my ($clname, $status) = ($w->[0], $w->[7]);
		if ($status eq $new_status) {
			warn "$clname already has status $status.\n";
			return;
		}
		$w->[7] = $new_status;
		return print "$clname status changed to $new_status.\n";
	} );
}

sub  enable($) { change_status(shift,  'enabled') }
sub disable($) { change_status(shift, 'disabled') }


#=======   Create/destroy   ============================================

sub create($$$$;$$$$$) {
	my ($clname, $netdev, $shelf, $slot,
	    $macaddr, $ipaddr, $status, $userpass, $comment) = @_;
	config_changer( $clname, sub {
		print "Client $clname already exists, skipped.\n";
		return;
	}, sub {
		execute "Clone disk",        $ZFS_CMD, 'clone',  $SOURCE_DRIVE,               "$DESTDRV_BASE/$clname";
		execute "Enable snapshots",  $ZFS_CMD, 'set',   'com.sun:auto-snapshot=true', "$DESTDRV_BASE/$clname";
		execute "Create AoE target", $AOE_ADM, 'create', $netdev, $shelf, $slot,      "$DESTDEV_BASE/$clname";
		print "Update configuration...\n";
		my @w;
		push @w, $clname, "$DESTDRV_BASE/$clname",
			$netdev, $shelf, $slot,
			$macaddr  // '-',
			$ipaddr   // '-',
			$status   // 'enabled',
			$userpass // '-',
			$comment  // '-';
		"@w";
	} );
}

sub destroy($) {
	config_reader($_[0], sub {
		my ($clname, $disk, $shelf, $slot) = ($_[0], $_[1], $_[3], $_[4]);
		execute "Remove $clname netdisk", $AOE_ADM, 'stop', "e$shelf.$slot";
		execute "Remove $clname partition", $ZFS_CMD, 'destroy', '-r', $disk;
	} );
}


#=======   Main   ======================================================

common_usage if @ARGV < 1;

sub Do {
	my($cmdname, $argscount, $usage, $cmdfunc, undef) = (@_);
	return if $ARGV[0] ne $cmdname;
	shift @ARGV;
	usage($usage) if ($argscount =~ /^\d+$/         and (@ARGV != $argscount));
	usage($usage) if ($argscount =~ /^(\d+)-(\d+)$/ and (@ARGV < $1 || @ARGV > $2));
	$cmdfunc->(@ARGV);
	exit;
}

Do 'list',        0, "list",                  \&status;
Do 'status',      0, "status",                \&status;

Do 'start',       1, "start <client_name>",   \&start;
Do 'stop',        1, "stop <client_name>",    \&stop;

Do 'start-all',   0, "start-all",             \&start_all;
Do 'stop-all',    0, "stop-all",              \&stop_all;
Do 'startall' ,   0, "startall",              \&start_all;
Do 'stopall',     0, "stopall",               \&stop_all;

Do 'enable',      1, "enable <client_name>",  \&enable;
Do 'disable',     1, "disable <client_name>", \&disable;

Do 'restart',     0, "restart",               \&restart;
Do 'reload',      0, "reload",                \&restart;

Do 'try-restart', 0, "try-restart",           \&cond_restart;
Do 'condrestart', 0, "condrestart",           \&cond_restart;
Do 'condreload',  0, "condreload",            \&cond_restart;

Do 'destroy',     1, "destroy <client_name>", \&destroy;
Do 'create',  '4-9', "create <client_name> netdev shelf slot [MAC-addr] [IP-addr] [enabled|disabled]",
                                              \&create;

usage "Unknown command: $ARGV[0]";

## EOF ##
