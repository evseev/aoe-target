#!/bin/bash
#
#  /etc/init.d/vblade -- startup script for OpenSuSE.
#
#  Written by ilya.evseev@gmail.com at Oct-2013
#
#  Published on https://bitbucket.org/evseev/aoe-target/
#
#  References:
#    - https://help.ubuntu.com/community/HighlyAvailableAoETarget
#    - http://en.opensuse.org/openSUSE:Packaging_init_scripts
#
### BEGIN INIT INFO
# Provides:             vblade
# Required-Start:       $network $local_fs $remote_fs
# Required-Stop:
# Default-Start:        2 3 4 5
# Default-Stop:         0 1 6
# Short-Description:    virtual AoE blade emulator
### END INIT INFO

set -e

name="vblade"

pid_dir="/var/run/$name"
logpath="/var/log/$name.log"
binpath="/usr/sbin/$name"
cfgpath="/etc/$name.conf"

# Config lines format:
#   network_device shelf slot file/disk/partition mac[,mac[,mac]] enabled/disabled
# Example:
#   bond0 0 1 /dev/drbd1

. /etc/rc.status ""   # "" workarounds _rc_todo assignment

RETVAL=0      # See http://en.opensuse.org/openSUSE:Packaging_init_scripts#Exit_Status_Codes
NOT_IMPLEMENTED=3
NOT_INSTALLED=5
NOT_CONFIGURED=6
NOT_RUNNING=7

test -x "$binpath" || { echo -n "Error: missing binary $binpath"; rc_status -s; exit $NOT_INSTALLED; }
test -d "$pid_dir" || mkdir -p "$pid_dir"

start_vblade() {
	# First 4 args are required.
	test -z "$1" -o -z "$2" -o -z "$3" -o -z "$4" && return 1

	local service_id="$1-e$2.$3"
	local pidfile="$pid_dir/$service_id.pid"
	local allowed_macaddrs=
	test "$5" = "" -o "$5" = "-" || allowed_macaddrs="-m $5"

	echo -n "Starting $name instance $service_id"

	# Service must be NOT disabled in config file..
	case "$6" in DISABLE* | Disable* | disable* ) rc_status -s; return 1;; esac

	# Network device MUST exist..
	/sbin/ip link list dev "$1" >/dev/null 2>&1 ||
		{ echo -n " .. $1 not found!"; rc_status -s; return 1; }

	# Image file/disk/partition MUST exist..
	test -r "$4" ||
		{ echo -n " .. $4 not found!"; rc_status -s; return 1; }

	pgrep -fF "$pidfile" "$binpath" >/dev/null 2>&1 &&
		{ echo -n " .. already running?";  rc_status -s; return 1; }

	# NB: We cannot use startproc because pidfile is not created..
	#startproc -l "$logpath" -p "$pidfile" -e "$binpath" $allowed_macaddrs $2 $3 $1 $4
	"$binpath" $allowed_macaddrs $2 $3 $1 $4 >> "$logpath" 2>&1 &
	local pid=$!
	rc_status -v
	echo "$pid" > "$pidfile"
}

start() {
	if ! test -r "$cfgpath"; then
		echo -n "Warning: missing config $cfgpath"
		rc_status -s
		RETVAL=$NOT_CONFIGURED
	fi

	local passed=
	while read line; do start_vblade ${line%%\#*} && passed="x$passed"; done < "$cfgpath"
	test "$passed" = "" && RETVAL=$NOT_RUNNING
	return 0  # ..without this, rc_exit returns 1
}

stop() {
	local passed=
	for pidfile in `find $pid_dir -maxdepth 1 -name "*.pid" -type f`; do
		echo -n "Stopping $name instance $(basename $pidfile .pid)"
		killproc -p $pidfile $binpath
		rc_status -v
		passed="x$passed"
	done

	test "$passed" = "" || return 0
	echo -n "Stop $name -- seems not running"
	rc_status -s
	RETVAL=$NOT_RUNNING
}

status() {
	local passed=
	for pidfile in `find $pid_dir -maxdepth 1 -name "*.pid" -type f`; do
		echo -n "Check $name instance $(basename $pidfile .pid)"
		checkproc -p "$pidfile" "$binpath"
		rc_status -v
		passed="x$passed"
	done

	test "$passed" = "" || return 0
	echo -n "$name is stopped."
	rc_status -s
	RETVAL=$NOT_RUNNING
}

is_running() {
	local passed=
	for pidfile in `find $pid_dir -maxdepth 1 -name "*.pid" -type f`; do
		checkproc -p "$pidfile" "$binpath"
		rc_status
		passed="x$passed"
	done
	test "$passed" = "" || return 0
	RETVAL=$NOT_RUNNING
	return $NOT_RUNNING
}

case "$1" in
	start ) start ;;
	stop  ) stop  ;;
	check | status ) status ;;
	restart | force-reload | reload ) stop; start ;;
	try-restart | condrestart | condreload ) is_running && { stop; start; } ;;
	*) echo $"Usage: ${0##*/} {start|stop|status|restart|try-restart|reload|force-reload}"
		RETVAL=$NOT_IMPLEMENTED
		;;
esac

test "$RETVAL" = "0" || exit $RETVAL  # ..workaround SuSE brainf*cking
rc_exit

## EOF ##
